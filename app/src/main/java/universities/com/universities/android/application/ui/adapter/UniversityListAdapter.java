package universities.com.universities.android.application.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import universities.com.universities.R;
import universities.com.universities.android.application.network.response.University;

/**
 * Created by rajesh_raje on 12/20/2016.
 */

public class UniversityListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<University> mUniversityList;

    private Context mContext;

    public UniversityListAdapter(Context context, List<University> universityList){

        this.mContext = context;
        this.mUniversityList = universityList;

    }

    private  class UniversityViewHolder extends RecyclerView.ViewHolder {

        TextView mUniversityName, mCountry;

        private LinearLayout item;

        public UniversityViewHolder(View itemView) {
            super(itemView);

            mUniversityName = (TextView) itemView.findViewById(R.id.university_name);
            mCountry = (TextView) itemView.findViewById(R.id.country);
            item = (LinearLayout) itemView.findViewById(R.id.itemView);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_university_list, parent, false);

        return new UniversityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        UniversityViewHolder universityViewHolder = (UniversityViewHolder) holder;

        universityViewHolder.mUniversityName.setText(mUniversityList.get(position).getName());
        universityViewHolder.mCountry.setText(mUniversityList.get(position).getCountry());

    }

    @Override
    public int getItemCount() {
        return mUniversityList.size();
    }
}
