package universities.com.universities.android.application.listener;

/**
 * Created by rajesh_raje on 12/20/2016.
 */

public interface UIListener<T> {

    public void onSuccess(T response);

    public void onFailure(String message);

}
