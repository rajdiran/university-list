package universities.com.universities.android.application.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

import universities.com.universities.android.application.listener.UIListener;
import universities.com.universities.android.application.network.response.University;

/**
 * Created by rajesh_raje on 12/20/2016.
 */

public class MyServiceManager {

    private static final String URL_GET_UNIVERSITIES = "http://universities.hipolabs.com/search?";

    public static void getUniviersitiesList(Context context, final UIListener<List<University>> uiListener) {

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonArrayRequest getUniversitiesRequest = new JsonArrayRequest(URL_GET_UNIVERSITIES,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Type type = new TypeToken<List<University>>() {
                        }.getType();

                        try {
                            List<University> array = new Gson().fromJson(response.toString(), type);
                            uiListener.onSuccess(array);
                        } catch (Exception e) {
                            uiListener.onFailure("Unable to retrieve university list from service. Please try again after some time!");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        uiListener.onFailure("Unable to retrieve university list from service. Please try again after some time!");
                    }
                }
        );

        queue.add(getUniversitiesRequest);

    }

}
