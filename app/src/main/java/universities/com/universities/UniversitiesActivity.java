package universities.com.universities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import universities.com.universities.android.application.listener.UIListener;
import universities.com.universities.android.application.network.MyServiceManager;
import universities.com.universities.android.application.network.response.University;
import universities.com.universities.android.application.ui.adapter.UniversityListAdapter;
import universities.com.universities.android.application.util.MyUtil;

public class UniversitiesActivity extends AppCompatActivity {

    private RecyclerView mUniversitiesList;

    private LinearLayout mErrorView;

    private TextView mErrorTitle, mErrorMessage, mRetryCTA;

    private ProgressBar mProgressBar;

    enum UI_STATE {
        NO_INTERNET, LOADING, SERVICE_ERROR, SUCCESS, EMPTY
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universities);

        initUI();
        updateUI();
    }

    private void initUI() {

        mUniversitiesList = (RecyclerView) findViewById(R.id.universities_list);
        mUniversitiesList.setLayoutManager(new LinearLayoutManager(UniversitiesActivity.this));

        mErrorView = (LinearLayout) findViewById(R.id.errorView);
        mErrorTitle = (TextView) findViewById(R.id.errorTitle);
        mErrorMessage = (TextView) findViewById(R.id.errorMessage);

        mRetryCTA = (TextView) findViewById(R.id.retryCTA);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mRetryCTA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUI();
            }
        });
    }

    private void updateUI() {

        if (!MyUtil.isInternetAvailable(UniversitiesActivity.this)) {
            updateUIState(UI_STATE.NO_INTERNET);
        } else {
            updateUIState(UI_STATE.LOADING);
            MyServiceManager.getUniviersitiesList(UniversitiesActivity.this, uiListener);
        }
    }

    UIListener<List<University>> uiListener = new UIListener<List<University>>() {
        @Override
        public void onSuccess(List<University> universityList) {

            if (null == universityList || universityList.isEmpty()) {
                updateUIState(UI_STATE.EMPTY);
            } else {
                feedUniversityToList(universityList);
                updateUIState(UI_STATE.SUCCESS);
            }
        }

        @Override
        public void onFailure(String message) {
            updateUIState(UI_STATE.SERVICE_ERROR);
        }
    };

    private void feedUniversityToList(List<University> universityList) {
        UniversityListAdapter universityListAdapter = new UniversityListAdapter(UniversitiesActivity.this, universityList);
        mUniversitiesList.setAdapter(universityListAdapter);
    }

    private void updateUIState(UI_STATE uiState) {
        switch (uiState) {
            case LOADING:
                mErrorView.setVisibility(View.GONE);
                mUniversitiesList.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case SUCCESS:
                showUniversityList(true);
                break;
            case NO_INTERNET:
                updateErrorMessage(getString(R.string.alert_InternetConnection_title),
                        getString(R.string.alert_InternetConnection_message), true);
                showUniversityList(false);
                break;
            case SERVICE_ERROR:
                updateErrorMessage(getString(R.string.alert_service_error_title),
                        getString(R.string.alert_service_error_message), true);
                showUniversityList(false);
                break;
            case EMPTY:
                updateErrorMessage(getString(R.string.no_university),
                        "", true);
                showUniversityList(false);
                break;
        }
    }

    private void updateErrorMessage(String title, String message, boolean showRetry) {
        mProgressBar.setVisibility(View.GONE);
        mRetryCTA.setVisibility(showRetry ? View.VISIBLE : View.GONE);
        mErrorTitle.setText(title);
        mErrorMessage.setText(message);
    }

    private void showUniversityList(boolean showList) {
        mProgressBar.setVisibility(View.GONE);
        mErrorView.setVisibility(showList ? View.GONE : View.VISIBLE);
        mUniversitiesList.setVisibility(showList ? View.VISIBLE : View.GONE);
    }

}
